//
//  SineOsc.cpp
//  JuceBasicAudio
//
//  Created by Joseph D'Souza on 17/11/2016.
//
//

#include "SineOsc.hpp"

SineOsc::SineOsc()
{
    DBG("Yay");
}

SineOsc::~SineOsc()
{
    
}

void SineOsc::void SineOsc::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{ (MidiInput* source, const MidiMessage& message)
{
    //All MIDI inputs arrive here
    DBG("midi");
    
    int vel = message.getVelocity();
    
    if(vel > 0)
    {
        Audio::setFrequency(message.getMidiNoteInHertz (message.getNoteNumber()));
        Audio::setMainAmp(vel*0.127);
    }
    else if(vel == 0)
    {
        Audio::setMainAmp(0);
        Audio::setFrequency(0);
    }
}