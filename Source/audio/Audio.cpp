/*
  ==============================================================================

    Audio.cpp
    Created: 13 Nov 2014 8:14:40am
    Author:  Tom Mitchell

  ==============================================================================
*/

#include "Audio.h"

Audio::Audio()
{
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); //2 inputs, 2 outputs
    audioDeviceManager.setMidiInputEnabled("Impulse  Impulse", true);
    
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.addAudioCallback (this);
    
    mainAmp = 0.;
}

Audio::~Audio()
{
    audioDeviceManager.removeAudioCallback (this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void Audio::setMainAmp(float amp)
{
    mainAmp = amp;
}

void Audio::setFrequency(float freq)
{
    frequency = freq;
}

void Audio::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    
}

void Audio::audioDeviceIOCallback (const float** inputChannelData,
                                           int numInputChannels,
                                           float** outputChannelData,
                                           int numOutputChannels,
                                           int numSamples)
{
    //All audio processing is done here
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    float synthOut;
    const float twoPi = 2 * M_PI;
    const float phaseIncrement = (twoPi * Audio::frequency.get())/sampleRate;
    
    while(numSamples--)
    {
        if (phasePosition > twoPi)
        {
            phasePosition = phasePosition - twoPi;
        }
        else
        {
            phasePosition = phasePosition + phaseIncrement;
        }
        
        synthOut = sinf(phasePosition);
        
        *outL = synthOut * mainAmp.get();
        *outR = synthOut * mainAmp.get();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}


void Audio::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("Start Audio");
    frequency = 440.f;
    phasePosition = 0.f;
    sampleRate = device->getCurrentSampleRate();
}

void Audio::audioDeviceStopped()
{

}